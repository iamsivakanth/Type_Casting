package com.training.java;

public class MainClass {

	public static void main(String[] args) {
		// Implicit
		byte b = 127;
		int i = b; // Type casting [int i = (int) b;]
		long l = b;
		System.out.println("byte value : " + b);
		System.out.println("int value : " + i);
		System.out.println("long value : " + l);

		// Explicit
		long x = 100;
		int y = (int) x;
		System.out.println("int value : " + y);

		byte z = (byte) y;
		System.out.println("byte value : " + z);

		long p = 130;
		int q = (int) p;
		byte r = (byte) q; // -128 to 127, 128 = -128, 129 = -127, 130 = -126
		System.out.println("byte value : " + r);
	}
}